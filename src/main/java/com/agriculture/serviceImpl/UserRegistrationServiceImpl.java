package com.agriculture.serviceImpl;
/**
 * @author koustubha
 */
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agriculture.customException.UserRegistrationFailed;
import com.agriculture.dao.UserRegistrationDao;
import com.agriculture.entity.UserRegistrationDetails;
import com.agriculture.service.UserRegistrationService;

@Service
public class UserRegistrationServiceImpl implements UserRegistrationService {

	@Autowired
	private UserRegistrationDao userRegistrationDao;

	@Override
	public void registerUser(UserRegistrationDetails userDetails) throws UserRegistrationFailed {
		
		try {
			userRegistrationDao.save(userDetails);
		}catch(HibernateException hb) {
			throw new UserRegistrationFailed(hb.getMessage());
		}
		
	}
}
