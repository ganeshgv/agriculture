package com.agriculture;
import org.modelmapper.ModelMapper;
/**
 * @author koustubha
 */
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.agriculture.*")
public class SpringAndReactApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringAndReactApplication.class, args);
	}
	
	/* Model mapper is used for DTO to DBO and DBO to DTO Conversions */
	
	@Bean
	public ModelMapper modelMapper() {
	    return new ModelMapper();
	}
}
