package com.agriculture.dao;
/**
 * @author koustubha
 */
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.agriculture.entity.UserRegistrationDetails;

@Repository
public interface UserRegistrationDao extends JpaRepository<UserRegistrationDetails, Integer> {
 
}
