package com.agriculture.customException;
/**
 * 
 * @author koustubha
 *
 */
public class UserRegistrationFailed extends Exception {

	public UserRegistrationFailed(String message) {
		super(message);
	}

}
