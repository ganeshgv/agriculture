package com.agriculture.controller;
/**
 * @author koustubha
 */
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

	@GetMapping("/api/sayHi")
	public String sayHi() {
		return "sayHi";
	}
}
