package com.agriculture.controller;

import org.modelmapper.ModelMapper;
/**
 * @author koustubha
 */
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.agriculture.dto.UserRegistrationDetailsDto;
import com.agriculture.entity.UserRegistrationDetails;
import com.agriculture.service.UserRegistrationService;

import net.sf.json.JSONObject;

/* This Controller consist all API'S related to USER Management  */

@RestController
public class UserController {

	@Autowired
	private UserRegistrationService userService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@PostMapping("/registerUser")
	public ResponseEntity<String> registerUser(@RequestBody UserRegistrationDetailsDto userDetailsDto) {
		
		JSONObject jsonObject = new JSONObject();
		HttpStatus status = HttpStatus.OK;
		
		try {
			UserRegistrationDetails userDetails = convertToEntity(userDetailsDto);
			userService.registerUser(userDetails);
			jsonObject.put("message", "User Registered Successfully !!");
		}catch(Exception e) {
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			jsonObject.put("message", e.getMessage());
		}
		
		return new ResponseEntity<String>(jsonObject.toString(), status);
	}
	
	private UserRegistrationDetails convertToEntity(UserRegistrationDetailsDto userDto) {
		
		UserRegistrationDetails userEntity = modelMapper.map(userDto, UserRegistrationDetails.class);
		return userEntity;
		
	}
}
