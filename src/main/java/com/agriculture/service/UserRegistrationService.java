package com.agriculture.service;

import com.agriculture.customException.UserRegistrationFailed;
import com.agriculture.entity.UserRegistrationDetails;

public interface UserRegistrationService {

	void registerUser(UserRegistrationDetails userDetails) throws UserRegistrationFailed;
}
